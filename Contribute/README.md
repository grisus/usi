# How to Contribute 

First, create a [GItLab](https://gitlab.com/users/sign_up) account, then open your terminal here and enter the following commands:
```bash
chmod +x config
./config 
```
Enter the password if promped, but do **NOT** run the command with sudo.
If Obsidian opens, everything should be fine. Otherwise, contact me on [GitLab](https://gitlab.com/grisus) or teams (guidel). 
Thank you for the collaboration.