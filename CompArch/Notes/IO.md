The [[IO]] known as Input and Output are an essential part of Computer systems are built up from three types of components: [[CPU]], [[Memory]], and I/O devices. The task of a processor is to fetch instructions one at a time from a memory, decode them, and execute them. The fetch-decode-execute cycle can always be described as an algorithm and, in fact, is sometimes carried out by a software interpreter running at a lower level. To gain speed, many computers now have one or more pipelines or have a superscalar design with multiple functional units that operate in parallel. A pipeline allows an instruction to be broken into steps and the steps for different instructions executed at the same time.
Multiple functional units is another way to gain parallelism without affecting the
instruction set or architecture visible to the programmer or compiler.

Systems with multiple processors are increasingly common. Parallel computers include array processors, on which the same operation is performed on multiple data sets at the same time, multiprocessors, in which multiple CPUs share a common memory, and multicomputers, in which multiple computers each have their own memories but communicate by message passing.

Memories can be categorized as primary or secondary. The primary memory
is used to hold the program currently being executed. Its access time is shorta few tens of nanoseconds at most and independent of the address being accessed.

Caches reduce this access time even more. They are needed because processor speeds are much greater than memory speeds, meaning that having to wait for memory accesses all the time greatly slows down processor execution. Some memories are equipped with error-correcting codes to enhance reliability.

Secondary memories, in contrast, have access times that are much longer (milliseconds or more) and dependent on the location of the data being read or written. Tapes, flash memory, magnetic disks, and optical disks are the most common secondary memories. Magnetic disks come in many varieties, including IDE disks, SCSI disks, and RAIDs. Optical disks include CD-ROMs, CDRs, DVDs, and Blu-rays.
I/O devices are used to transfer information into and out of the computer.
They are connected to the processor and memory by one or more buses. Examples are terminals, mice, game controllers, printers, and modems. Most I/O devices use the ASCII character code, although Unicode is also used and UTF-8 is gaining acceptance as the computer industry becomes more Web-centric

## Representing text
We assign numerical values to singular characters:
there have been numerous attempts at developing a universal conversion factor:
- **ASCII** (7 bit + 1 parity bit) - old terminals, extremely limited
(this needs a 0 at the end of every string to conclude it. The main failure of this language is precisely its strength, so the 0 is both very efficient, since in ASCII it doesn’t mean anything, but programmers could forget the 0, so a READ, if there wasn’t any other 0 in the memory, could cover the whole memory. To solve this, every string would start with a byte or more that represented the string length, so that a READ would not cover the whole memory(Its still limited to 255 characters, if only 1 byte was mentioned, but it was deemed enough)
- **ISO 8859** (8 bit) - terminals become more reliable, no need for a parity bit
anymore, yet more bits needed to represent other characters
- **UNICODE** (16 bit) - 2 16 possible representations, yet now every character is 16-bit long, and they’re not enough combinations
- **UCS Transformation Format (UTF-8)**: is the latest conversion language and supports almost every character. this is accomplished by assigning an arbitrary
number of bits to every character, to avoid waste of bits:
	- (numbers from 0-127 are considered ASCII must be ASCII)
⇒ If the byte starts with a 0, it
	- (if it starts with 1: 110 means its going to use 2 bytes: 1110 means its going to use 3 bytes: etc… . I it starts with 10, it means that that is a follow-up bit of one of the cases previously mentioned.)
	- (1920 characters could be expressed with 2 bytes. With the “head byte” starting with 1110xxx, you could represent up to 2.097.152 characters.)