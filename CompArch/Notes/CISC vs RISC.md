
## Complex Instruction Set Computer

-   **CISC**: **Large # of complex instructions**
-   Properties:
    -   Complex **ISA instructions are broken down into multiple simple microinstructions**
    -   Not only load/store but also **other ISA instructions access memory**
    -   Retroactively coined term usually means “not RISC”
-   Historic and Current Examples
    -   DEC VAX (1977, 32-bit CISC)
    -   Intel/AMD x86 architecture

## Reduced Instruction Set Computer

-   **RISC: Small # of simple instructions**
    -   Coined by Patterson/Séquin at UC Berkeley
-   Properties
    -   **ISA instructions = microinstructions**
    -   Only load/store instructions access memory
-   Historic and Current Examples
    -   Patterson: UC Berkeley RISC-I/RISC-II → Sun SPARC (‘87)
    -   Hennessy: Stanford MIPS → MIPS MIPS (‘81) → SGI MIPS
    -   ARM (Acorn): Advanced RISC Machine
        -   Apple A4-A10, Qualcomm (Snapdragon), Samsung Exynos, TIOMAP, Nvidia Tegra, …

### RISC Design Principles

-   All ISA instructions directly executed on hardware
-   Only loads and stores should reference memory
-   Provide plenty of registers
-   Maximize rate at which instructions are issued
    -   Covered later in this course
-   Instructions should be easy to decode
    -   Covered later in this course

## RISC vs CISC?

-   Which one is better?
    -   **RISC: typically faster, more efficient**
    -   **CISC: offers backward compatibility! (e.g., Intel)**
-   Which is more widespread?
    -   Mobile: **RISC**
        -   ARM: low-power efficiency as starting point
    -   Desktop/Laptop: **CISC***
        -   Intel: backward compatibility as key requirement
        -   *CISC systems today incorporate RISC elements
![[CISCvsRISC.png]]
