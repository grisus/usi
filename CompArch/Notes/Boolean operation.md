# Boolean Operations 
Boolean operations are calculated inside the [[ALU]] 

## Logic Gates
The operation are made using the _boolean algebra_ through what are called _logic gates_, where *A* and *B* are the inputs and *X* is the output 
![[logic_gates.png]]

The basic gates makes the following algebraic functions/operators 
![[Basic_gates.png]]
With the combination of those basic gates we can make every possible arithmetic operation inside the [[CPU]]

Level of the multilanguage machine:
![[boolean.png]]

The NAND and NOR are defined as "Completeness Gates" because you can derive all other gates from those two such as the AND, NOT and OR: 
![[completeness_gates.png]]

But what are transistors? 
A typical transistor provides the function of "Switching", where the base current triggers current across emitter and collector but it can simply be said that it acts as a NOT gate. 

## Boolean Algebra 
To present the outputs in a more cleaner way we use whats called Boolean Algebra: 
![[boolean_algebra.png]]
