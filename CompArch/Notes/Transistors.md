# Transistors
Transistor are simple electronic devices with an essential pinout:
- Base, where a small current flows in;
- Collector, where the mail Vcc runs;
- Emitter, where current flows to the ground only if B and C are triggered.
___
![[NPN.jpg|300]]
___
A single Transistor is essentially a NOT [logic gate](Boolean operation#Logic Gates). if combined with other little friends, we can get more interesting stuff. The [[CPU]] is made out of billion of Transistors
___
![[TransGates.jpg]]
___
The two gates NAND and NOR are called 'functionally complete', because every other logic gate can be build out of them.